# 1.0.0 (2024-08-12)


### Bug Fixes

* set namespace for helmrepo ([ccf9778](https://gitlab.com/kadse-xyz/flux-control/shared/commit/ccf97782593f250e00be5b428c6ceb7314ed9625))


### Features

* add bots and config ([98c7b39](https://gitlab.com/kadse-xyz/flux-control/shared/commit/98c7b39a9d392ba598226ad8140531264a3d727a))
* add renovate ([e402a41](https://gitlab.com/kadse-xyz/flux-control/shared/commit/e402a41a401dfe1097e624528ab920cd604e3e18))
* create bitnami helmrepo ([24dcaae](https://gitlab.com/kadse-xyz/flux-control/shared/commit/24dcaae482526f109245934e7cdbc3afba02788f))
