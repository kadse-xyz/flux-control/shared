module.exports = {
    branches: [
        "main"
    ],
    plugins: [
        "@semantic-release/release-notes-generator",
        [
            "@semantic-release/changelog",
            {
                changelogFile: "CHANGELOG.md"
            }
        ],
        [
            "@semantic-release/gitlab",
            {
                gitlabUrl: process.env.GITLAB_URL,
                assets: [
                    {
                        path: "CHANGELOG.md"
                    }
                ]
            }
        ],
        [
            "@semantic-release/git",
            {
                assets: [
                    "CHANGELOG.md"
                ],
                message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
            }
        ]
    ]
};
